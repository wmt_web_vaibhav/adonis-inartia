import React, { useState } from "react";

import { InertiaLink } from "@inertiajs/inertia-react";

import { Layout, Menu } from "antd";
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";

import "./index.css";

const { Header, Content, Footer, Sider } = Layout;

function AuthLayout(props) {
  return (
    <Layout style={{ height: "100vh" }}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={window.location.pathname}
        >
          <Menu.Item key="/" icon={<UserOutlined />}>
            <InertiaLink href="/">Dashboard</InertiaLink>
          </Menu.Item>
          <Menu.Item key="/user" icon={<VideoCameraOutlined />}>
            <InertiaLink href="/user">Users</InertiaLink>
          </Menu.Item>
          <Menu.Item key="3" icon={<UploadOutlined />}>
            About Us
          </Menu.Item>
          <Menu.Item key="4" icon={<UserOutlined />}>
            Contact Us
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header
          className="site-layout-sub-header-background"
          style={{ padding: 0 }}
        />
        <Content style={{ margin: "24px 16px 0" }}>
          <div
            className="site-layout-background"
            style={{ padding: 24, height: "100%" }}
          >
            {props.children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
}

export default AuthLayout;
