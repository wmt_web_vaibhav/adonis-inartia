import React from "react";
import AuthLayout from "../../Layout/AuthLayout";

import { Table } from "antd";

const columns = [
  {
    title: "First Name",
    dataIndex: "first_name",
    key: "first_name",
  },
  {
    title: "Last Name",
    dataIndex: "last_name",
    key: "last_name",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
];

function User(props) {
  const data = props.users;

  return (
    <div>
      <AuthLayout>
        <Table columns={columns} dataSource={data} />
      </AuthLayout>
    </div>
  );
}

export default User;
