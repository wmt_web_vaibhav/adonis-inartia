import React from "react";
import AuthLayout from "../../Layout/AuthLayout";

import { Card, Avatar } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from "@ant-design/icons";

const { Meta } = Card;

function Dashboard(props) {
  const users = props?.users;

  return (
    <AuthLayout>
      {users &&
        users.map((data) => {
          return (
            <Card
              style={{ width: 300 }}
              cover={
                <img
                  alt="example"
                  src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                />
              }
              actions={[
                <SettingOutlined key="setting" />,
                <EditOutlined key="edit" />,
                <EllipsisOutlined key="ellipsis" />,
              ]}
            >
              <Meta
                avatar={
                  <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                }
                title={data?.first_name}
                description={data?.email}
              />
            </Card>
          );
        })}
    </AuthLayout>
  );
}

export default Dashboard;
